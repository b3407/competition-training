package xyz.chenprime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersonalModuleApplication {

    public static void main(String[] args) {
        SpringApplication.run(PersonalModuleApplication.class, args);
    }

}
