package xyz.chenprime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccessModuleApplication {

    public static void main(String[] args) {
        SpringApplication.run(AccessModuleApplication.class, args);
    }

}
