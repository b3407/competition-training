package xyz.chenprime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReleaseModuleApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReleaseModuleApplication.class, args);
    }

}
