package xyz.chenprime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DailyModuleApplication {

    public static void main(String[] args) {
        SpringApplication.run(DailyModuleApplication.class, args);
    }

}
